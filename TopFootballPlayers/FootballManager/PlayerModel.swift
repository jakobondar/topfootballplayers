//
//  PlayerModel.swift
//  TopFootballPlayers
//
//  Created by Яков on 27.10.2021.
//

import Foundation

struct Player {
    var firstName: String
    var height: String
    var lastName: String
    var nationality: String
    var teamName: String
    var photoPlayer: String
    var logoClub: String
}

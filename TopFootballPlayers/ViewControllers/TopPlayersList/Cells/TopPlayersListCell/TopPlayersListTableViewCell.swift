//
//  TopPlayersListTableViewCell.swift
//  TopFootballPlayers
//
//  Created by Яков on 27.10.2021.
//

import UIKit

class TopPlayersListTableViewCell: UITableViewCell {

    @IBOutlet weak var imagePlayer: UIImageView!
    @IBOutlet weak var logoClubImage: UIImageView!
    @IBOutlet weak var firstNameLabel: UILabel!
    @IBOutlet weak var heightLabel: UILabel!
    @IBOutlet weak var lastNameLabel: UILabel!
    @IBOutlet weak var nationalityLabel: UILabel!
    @IBOutlet weak var taemNaneLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func update(_ player: Player) {
        firstNameLabel.text = player.firstName
        heightLabel.text = player.height
        lastNameLabel.text = player.lastName
        nationalityLabel.text = player.nationality
        taemNaneLabel.text = player.teamName
        getImage(urlImage: player.photoPlayer, photo: imagePlayer)
        getImage(urlImage: player.logoClub, photo: logoClubImage)
    }
    
    func getImage(urlImage: String, photo: UIImageView) {
        
        if let url = URL(string: urlImage) {
            let task = URLSession.shared.dataTask(with: url) { data, response, error in
                guard let data = data, error == nil else { return }
                
                DispatchQueue.main.async { /// execute on main thread
                    photo.image = UIImage(data: data)
                }
            }
            task.resume()
        }
    }
    
}



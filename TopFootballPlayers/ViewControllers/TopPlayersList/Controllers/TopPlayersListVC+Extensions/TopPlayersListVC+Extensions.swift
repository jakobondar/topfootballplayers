//
//  TopPlayersListVC+Extensions.swift
//  TopFootballPlayers
//
//  Created by Яков on 27.10.2021.
//

import UIKit

//MARK: add Table View
extension TopPlayersListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 35))
        let label = UILabel(frame: view.frame)
        label.backgroundColor = .systemBlue
        label.font = UIFont.boldSystemFont(ofSize:UIFont.labelFontSize)
        label.font = UIFont.boldSystemFont(ofSize: 18)
        label.text = "Топчики по пинаниям мяча:"
        label.textColor = .white
        label.textAlignment = .center
        view.addSubview(label)
        return view
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return topPlayersList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: topPlayersListCellID, for: indexPath) as! TopPlayersListTableViewCell
        cell.update(topPlayersList[indexPath.row])
        return cell
    }
}

//MARK: add func readJsonFile
extension TopPlayersListViewController {
    
    func readJsonFile(league: String, season: String) {
       
        var urlComponent = URLComponents(string: "https://api-football-beta.p.rapidapi.com/players/topscorers")!
        let parameterCountry = URLQueryItem(name: "league", value: league)
        let parameterSeason = URLQueryItem(name: "season", value: season)
        
        urlComponent.queryItems = [parameterCountry, parameterSeason]
        
        var request = URLRequest(url: urlComponent.url!)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = ["x-rapidapi-host": "api-football-beta.p.rapidapi.com",
                                       "x-rapidapi-key": "251b4eb8c7mshc148c7ffba6d3c8p1ff79fjsnc7edda17d876"]
        
        URLSession.shared.dataTask(with: request) { data, response, error in
            
            if let error = error {
                print(error.localizedDescription)
            }
            
            if let data = data {
                do {
                    if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                       
                        if let responseKey = json["response"] as? [[String: Any]] {
                        
                            for object in responseKey {
                                
                                var generateNewPlayer: Player = .init(firstName: "", height: "", lastName: "", nationality: "", teamName: "", photoPlayer: "", logoClub: "")
                            
                                if let players = object["player"] as? [String: Any] {
                                
                                    if let firstName = players["firstname"] as? String,
                                       let height = players["height"] as? String,
                                       let lastName = players["lastname"] as? String,
                                       let nationality = players["nationality"] as? String,
                                       let photoPlayer = players["photo"] as? String {
                                        
                                        generateNewPlayer.firstName = firstName
                                        generateNewPlayer.height = height
                                        generateNewPlayer.lastName = lastName
                                        generateNewPlayer.nationality = nationality
                                        generateNewPlayer.photoPlayer = photoPlayer
                                    }
                                }
                                
                                if let statistics = object["statistics"] as? [[String: Any]] {
                                    
                                    for playerStatistic in statistics {
                                        
                                        if let team = playerStatistic["team"] as? [String: Any] {
                                            
                                            if let teamName = team["name"] as? String,
                                               let logo = team["logo"] as? String {
                                                
                                                generateNewPlayer.teamName = teamName
                                                generateNewPlayer.logoClub = logo
                                                
                                            }
                                        }
                                    }
                                }
                                self.topPlayersList.append(generateNewPlayer)
                            }
                            
                            DispatchQueue.main.async {
                                self.tableView.reloadData()
                            }
                            print(self.topPlayersList.count)
                        }
                    }
                } catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }.resume()
    }
}


//
//  TopPlayersListViewController.swift
//  TopFootballPlayers
//
//  Created by Яков on 27.10.2021.
//

import UIKit

class TopPlayersListViewController: UIViewController {

    let topPlayersListCellID = String(describing: TopPlayersListTableViewCell.self)
    var topPlayersList = [Player]()
    var season: String = "2019"
    var league: String = "39"
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: topPlayersListCellID, bundle: nil), forCellReuseIdentifier: topPlayersListCellID)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        readJsonFile(league: league, season: season)
    }
}
